#include <cmath>
#include <cstring>
#include <complex>
#include <vector>

typedef std::complex<double> base;

const double PI = 3.14159265358979323846;

void _copySync(base* dest, const base* src, int n) {
    std::memcpy(dest, src, sizeof(base) * n);
}

base* initSinCosSync(int n) {
    double arg, step;
    int i, m = n / 2;
    base* vc = new base[m];
    step = 2.0 * PI / n;

    for (i = 0; i < m; i++) {
        arg = step * i;
        vc[i] = base(std::cos(arg), std::sin(arg));
    }
    return vc;
}

void partialSync(int n, int offs, base* a, base* b, base* w, int sgn) {
    int i, ia, ib, iw, k, count;
    base *al = a, *ar = a + n/2, *bl = b, *br = b + offs;
    base temp, wiw;
    count = n / offs / 2;
    for (i = 0; i < count; i++) {
        iw = i * offs;
        ia = iw;
        ib = i * offs * 2;
        wiw = w[iw];
        if (sgn < 0) {
            wiw = std::conj(wiw);
        }
        for (k = 0; k < offs; k++) {
            bl[ib+k] = al[ia+k] + ar[ia+k];
            temp = al[ia+k] - ar[ia+k];
            br[ib+k] = wiw * temp;
        }
    }
}

void fftNoOpenMP(int n, base* x, base* y, int sgn, base* w = nullptr) {
    int i, m = (int)(std::log(n) / std::log(2)), mj = 1;
    bool wCreated = false;
    if (!w) {
        w = initSinCosSync(n);
        wCreated = true;
    }
    partialSync(n, mj, x, y, w, sgn);

    if (n == 2) {
        return;
    }

    for (i = 0; i < m - 2; i++) {
        mj *= 2;
        if (!(i & 1)) {
            partialSync(n, mj, y, x, w, sgn);
        }
        else {
            partialSync(n, mj, x, y, w, sgn);
        }
    }
    if (!(i & 1)) {
        _copySync(x, y, n);
    }
    mj = n / 2;
    partialSync(n, mj, x, y, w, sgn);
    if (wCreated) {
        delete[] w;
    }
    if (sgn < 0) {
        for (i = 0; i < n; i++) {
            y[i] /= n;
        }
    }
}
