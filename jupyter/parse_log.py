import pandas as pd
import numpy as np


def create_log_df(filename, end):
    lines = []
    with open(filename) as log:
        for line in log:
            if line.startswith('Mem'):
                lines.append(line.split()[1:])
        df = pd.DataFrame(lines, columns=[
            'total', 'used', 'free',
            'shared', 'buff/cache', 'available'
        ])
        start = pd.Timestamp(end) - pd.Timedelta(f'{len(lines)} sec')
        df.index = [start + pd.Timedelta(f'{i} sec') for i in range(len(df))]
        df.to_csv(f'{filename[:-4]}.csv')