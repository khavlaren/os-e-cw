//
// https://rosettacode.org/wiki/Fast_Fourier_transform
//

#include <complex>
#include <valarray>

const double PI = 3.141592653589793238460;

typedef std::complex<double> base;
typedef std::valarray<base> CArray;

base* init(int n);

// Basic variant.
void fftHard(CArray& x)
{
    const size_t N = x.size();
    if (N <= 1) return;

    CArray even = x[std::slice(0, N/2, 2)];
    CArray  odd = x[std::slice(1, N/2, 2)];

    fftHard(even);
    fftHard(odd);

    for (size_t k = 0; k < N/2; ++k)
    {
        base t = std::polar(1.0, -2 * PI * k / N) * odd[k];
        x[k] = even[k] + t;
        x[k+N/2] = even[k] - t;
    }
}

// Optimized (as a man on the website say).
void fftLight(CArray &x)
{
    unsigned int N = x.size(), k = N, n;
    double thetaT = PI / N;
    base phiT = base(cos(thetaT), -sin(thetaT)), T;
    while (k > 1)
    {
        n = k;
        k >>= 1;
        phiT = phiT * phiT;
        T = 1.0L;
        for (unsigned int l = 0; l < k; l++)
        {
            for (unsigned int a = l; a < N; a += n)
            {
                unsigned int b = a + k;
                base t = x[a] - x[b];
                x[a] += x[b];
                x[b] = t * T;
            }
            T *= phiT;
        }
    }
    auto m = (unsigned int)log2(N);
    for (unsigned int a = 0; a < N; a++)
    {
        unsigned int b = a;
        // Reverse bits
        b = (((b & 0xaaaaaaaa) >> 1) | ((b & 0x55555555) << 1));
        b = (((b & 0xcccccccc) >> 2) | ((b & 0x33333333) << 2));
        b = (((b & 0xf0f0f0f0) >> 4) | ((b & 0x0f0f0f0f) << 4));
        b = (((b & 0xff00ff00) >> 8) | ((b & 0x00ff00ff) << 8));
        b = ((b >> 16) | (b << 16)) >> (32 - m);
        if (b > a)
        {
            base t = x[a];
            x[a] = x[b];
            x[b] = t;
        }
    }
}

void ifftLight(CArray& x)
{
    x = x.apply(std::conj);
    fftLight( x );
    x = x.apply(std::conj);
    x /= x.size();
}

void ifftHard(CArray& x)
{
    x = x.apply(std::conj);
    fftHard( x );
    x = x.apply(std::conj);
    x /= x.size();
}