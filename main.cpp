#include <iostream>
#include <cmath>
#include <complex>
#include <valarray>
#include <vector>
#include <chrono>
#include <fstream>
#include <valarray>

typedef std::complex<double> base;

void fftOpenMP(int n, base* x, base* y, int sgn, base* w = nullptr);
void fftNoOpenMP(int n, base* x, base* y, int sgn, base* w = nullptr);

void fftHard(std::valarray<base>& x);
void ifftHard(std::valarray<base>& x);
void fftLight(std::valarray<base>& x);
void ifftLight(std::valarray<base>& x);


const double PI = 3.14159265358979323846;
const int START = 32;
const int END = 1024 * 1024 * 32;

base* init(int n) {
    base* x = new base[n];
    int i;
#pragma omp parallel shared(x, n) private(i) default(none)
#pragma omp for nowait
    for (i = 0; i < n; i++) {
        x[i] = base(i, -i);
    }
    return x;
}

void printElapsed(long elapsed) {
    if (elapsed < 1000) {
        std::cout << elapsed << " microseconds.";
    }
    else if (elapsed < 1000000) {
        std::cout << elapsed / 1000.0 << " milliseconds.";
    }
    else {
        std::cout << elapsed / 1000000.0 << " seconds.";
    }
    std::cout << std::endl;
}

void printDuration(std::chrono::high_resolution_clock::time_point tStart,
        std::chrono::high_resolution_clock::time_point tEnd,
        std::vector<long>* times = nullptr, int numOfElements = -1) {
    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(tEnd - tStart).count();
    printElapsed(elapsed);
    if (times && ~numOfElements) {
        int row = int(std::log(numOfElements/START) / std::log(2));
        times[row].push_back(elapsed);
    }
}

void checkError(base* xTrue, base* xCalc, int n) {
    double error = 0.0;
    for (int i = 0; i < n; i++)
    {
        error += std::pow(std::abs(xCalc[i] - xTrue[i]), 2);
    }
    std::cout << "MSE: " << error / n << "\n\n";
}

void createCSV(std::vector<long>* results, int numOfTests, int numOfRuns) {
    int i, j;
    std::time_t t = std::time(nullptr);
    std::tm tm = *std::localtime(&t);
    std::ofstream output;
    char path[33];
    std::strftime(path, sizeof(path), "../tests/%F-%T.csv\0", &tm);
    output.open(path);
    for(i = 0; i < numOfRuns*2 - 1; i++) {
        if (i & 1) {
            output << "IFFT_";
        }
        else {
            output << "FFT_";
        }
        output << i / 2 << ',';
    }
    output << "IFFT_" << i / 2 << '\n';
    for (i = 0; i < numOfTests; i++) {
        for (j = 0; j < numOfRuns*2 - 1; j++) {
            output << results[i][j] << ",";
        }
        output <<  results[i][j] << '\n';
    }
    output.close();
}

int main() {
    int n, i, numOfTests = int(std::log(END/START) / std::log(2)) + 1, numOfRuns = 100;
    auto* results = new std::vector<long>[numOfTests];
    std::chrono::high_resolution_clock::time_point tStart, tEnd;

    for (n = START; n <= END; n *= 2) {
        base *x = init(n), *y = new base[n], *z = init(n);
        std::cout << "Number of elements: " << n << ".\n---------------------------------\n";

        for (i = 0; i < numOfRuns; i++) {
            tStart = std::chrono::high_resolution_clock::now();
            // std::valarray<base> xx(x, n);
            // fftHard(xx);
            fftOpenMP(n, x, y, 1);
            tEnd = std::chrono::high_resolution_clock::now();
            std::cout << "FFT: Duration is ";
            printDuration(tStart, tEnd, results, n);

            tStart = std::chrono::high_resolution_clock::now();
            fftOpenMP(n, y, x, -1);
            // ifftHard(xx);
            tEnd = std::chrono::high_resolution_clock::now();
            std::cout << "IFFT: Duration is ";
            printDuration(tStart, tEnd, results, n);
            // checkError(z, x, n);
        }
        std::cout << std::endl;
        delete[] x;
        delete[] y;
        delete[] z;
    }
    createCSV(results, numOfTests, numOfRuns);

    delete[] results;
}
